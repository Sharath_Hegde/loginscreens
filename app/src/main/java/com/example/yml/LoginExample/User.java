package com.example.yml.LoginExample;

/**
 * Created by sharath on 6/9/17.
 */

public class User {
    private String mUserName;
    private String mEmail;
    private String mPassWord;

    public void setmUserName(String mUserName) {
        this.mUserName = mUserName;
    }

    public void setmEmail(String mEmail) {
        this.mEmail = mEmail;
    }

    public void setmPassWord(String mPassWord) {
        this.mPassWord = mPassWord;
    }

    public String getmUserName() {

        return mUserName;
    }

    public String getmEmail() {
        return mEmail;
    }

    public String getmPassWord() {
        return mPassWord;
    }

    public User(String mUserName, String mEmail, String mPassWord) {
        this.mUserName = mUserName;

        this.mEmail = mEmail;
        this.mPassWord = mPassWord;
    }



}
