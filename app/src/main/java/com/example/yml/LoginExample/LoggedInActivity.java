package com.example.yml.LoginExample;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.HashMap;


public class LoggedInActivity extends AppCompatActivity {


    private TextView txtName;
    private TextView txtEmail;
    private Button btnLogout;
    private SessionManager session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logged_in);

        txtName = (TextView) findViewById(R.id.user_name);
        txtEmail = (TextView) findViewById(R.id.user_email);
        btnLogout = (Button) findViewById(R.id.btnLogout);

        session = new SessionManager(getApplicationContext());

        HashMap<String, String> user = session.getUserDetails();

        String tempName = user.get(SessionManager.KEY_NAME);

        String tempEmail = user.get(SessionManager.KEY_EMAIL);


        txtName.setText(tempName);
        txtEmail.setText(tempEmail);

        btnLogout.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                logoutUser();
            }
        });
    }

    private void logoutUser() {
        session.setLogin(false);

        Intent intent = new Intent(LoggedInActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }
}
