package com.example.yml.LoginExample;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by sharath on 6/9/17.
 */

public class DatabaseHandler extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 2;

    private static final String DATABASE_NAME = "userManager";

    private static final String TABLE_NAME = "USERS";

    private static final String KEY_ID = "id";
    private static final String KEY_NAME = "name";
    private static final String KEY_EMAIL = "email";
    private static final String KEY_PASSWORD = "password";


    private static final java.lang.String CREATE_TABLE = "CREATE TABLE "+TABLE_NAME+" ( "+KEY_ID+
            " INTEGER PRIMARY KEY AUTOINCREMENT ,"+KEY_NAME+" TEXT ,"
            +KEY_EMAIL+" TEXT ,"+KEY_PASSWORD+" TEXT );" ;


    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

        sqLiteDatabase.execSQL(CREATE_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

        sqLiteDatabase.execSQL("DROP TABLE"+TABLE_NAME);

        onCreate(sqLiteDatabase);
    }

     public void addContact(User user) {
        SQLiteDatabase db = this.getWritableDatabase();

       ContentValues values = new ContentValues();
        values.put(KEY_NAME, user.getmUserName());
        values.put(KEY_EMAIL, user.getmEmail());
        values.put(KEY_PASSWORD, user.getmPassWord());

        db.insert(TABLE_NAME, null, values);
        db.close();
    }

    User getContact() {
        SQLiteDatabase db = this.getWritableDatabase();
        User user = null;

        Cursor cursor = db.rawQuery("SELECT * FROM "+TABLE_NAME,null);

        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();

            String name = cursor.getString(cursor.getColumnIndex(KEY_NAME));
            String email = cursor.getString(cursor.getColumnIndex(KEY_EMAIL));
            String password = cursor.getString(cursor.getColumnIndex(KEY_PASSWORD));

           user = new User(name,email,password);
        }

        return user;
    }



}
