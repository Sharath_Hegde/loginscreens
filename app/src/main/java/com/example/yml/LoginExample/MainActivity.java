package com.example.yml.LoginExample;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    private EditText mEmail;
    private EditText mPassword;
    private Button mSignIn;
    private Button mLinkToSignUp;
    private SessionManager session;
    private DatabaseHandler db;
    private String userManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mEmail = (EditText) findViewById(R.id.login_email);

        mPassword = (EditText) findViewById(R.id.login_password);

        mSignIn = (Button) findViewById(R.id.btn_login);

        mLinkToSignUp = (Button) findViewById(R.id.link_to_signup);

        mSignIn.setOnClickListener(this);
        mLinkToSignUp.setOnClickListener(this);


        db = new DatabaseHandler(this);
        session = new SessionManager(this);


        if (session.isLoggedIn()) {

            Intent intent = new Intent(MainActivity.this, LoggedInActivity.class);
            startActivity(intent);
            finish();
        }




    }

    @Override
    public void onClick(View view) {


        switch (view.getId())
        {
            case R.id.btn_login:

                String email = mEmail.getText().toString().trim();
                String password = mPassword.getText().toString().trim();



                if (!email.isEmpty() && !password.isEmpty()) {

                    checkLogin(email, password);

                } else {

                    Toast.makeText(getApplicationContext(),
                            "Please enter the credentials!", Toast.LENGTH_LONG)
                            .show();
                }


                break;

            case R.id.link_to_signup:

                Intent intent = new Intent(MainActivity.this, SignUpActivity.class);
                startActivity(intent);
                finish();

                break;
        }


    }

    private void checkLogin(String email, String password) {

        String tempEmail="";
        String tempPassword="";


        //Sign in using shared preference

          /*  HashMap<String, String> user = session.getUserDetails();

          String tempPassword = user.get(SessionManager.KEY_PASSWORD);

           String tempEmail = user.get(SessionManager.KEY_EMAIL); */


      //Sign in using sqlite




        User user = db.getContact();
        if(user != null){

            tempEmail = user.getmEmail();

          tempPassword = user.getmPassWord();

        }
        if(TextUtils.isEmpty(tempEmail)&& TextUtils.isEmpty(tempPassword))
        {

            Toast.makeText(getApplicationContext(),
                    "Do Signup first!", Toast.LENGTH_LONG)
                    .show();


        }
        else if(tempEmail.equals(email)&& tempPassword.equals(password)){

            Intent intent = new Intent(MainActivity.this, LoggedInActivity.class);
            startActivity(intent);
            finish();
        }

        else {

            Toast.makeText(getApplicationContext(),
                    "Wrong Credentials", Toast.LENGTH_LONG)
                    .show();

        }


    }
}
