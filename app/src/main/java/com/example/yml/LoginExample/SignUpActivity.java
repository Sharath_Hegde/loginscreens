package com.example.yml.LoginExample;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class SignUpActivity extends AppCompatActivity implements View.OnClickListener{

    private EditText mEmail;
    private EditText mName;
    private EditText mPassword;
    private Button mSignUp;
    private Button mLinkToLogin;
    private SessionManager session;
    private DatabaseHandler db;
    private List<User> userList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        mName = (EditText) findViewById(R.id.name);

        mEmail = (EditText) findViewById(R.id.signup_email);

        mPassword = (EditText) findViewById(R.id.signup_password);

        mSignUp = (Button) findViewById(R.id.btnRegister);

        mLinkToLogin = (Button) findViewById(R.id.btnLinkToLoginScreen);

        mSignUp.setOnClickListener(this);
        mLinkToLogin.setOnClickListener(this);


        db = new DatabaseHandler(this);
        session = new SessionManager(getApplicationContext());


    }

    @Override
    public void onClick(View view) {

        switch (view.getId())
        {
            case R.id.btnRegister:

                String name = mName.getText().toString().trim();

                String email = mEmail.getText().toString().trim();
                String password = mPassword.getText().toString().trim();

                if (!name.isEmpty() && !email.isEmpty() && !password.isEmpty()) {

                    session.createLoginSession(name,email,password);
                    db.addContact(new User(name,email,password));

                    //Storing user detail in memory
                    userList = new ArrayList<>();
                    userList.add(new User(name,email,password));

                    Intent i = new Intent(getApplicationContext(), LoggedInActivity.class);
                    startActivity(i);
                    finish();


                } else {

                    Toast.makeText(getApplicationContext(),
                            "Please enter the credentials!", Toast.LENGTH_LONG)
                            .show();
                }


                break;

            case R.id.btnLinkToLoginScreen:

                Intent intent = new Intent(SignUpActivity.this, MainActivity.class);
                startActivity(intent);
                finish();

                break;
        }


    }
}
